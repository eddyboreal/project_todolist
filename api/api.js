"use strict";

const express = require('express')
const router = express.Router()
const data = require('../db/data.js')
//api: informations sur un utilisateur

// récupérer les informations pour un utilisateur      EST CE UTILE ?  
router.get('/user', (req, res) => {
    //const users = acces.getAllUsers();
    res.header("Access-Control-Allow-Origin", "*");
    console.log("on est dans lulu_d")
    console.log(req.session.username)

    data.executeQuery('Select * from Utilisateur u, Liste l, Tache t  WHERE u.pseudonyme = $1 AND u.id_utilisateur = l.id_utilisateur AND l.id_liste = t.id_liste' , [req.session.username], (result) => {
        res.json({user: result.rows})
    })
})

//informations sur les listes d'un utilisateur
router.get('/user/list/', (req, res) => {
    console.log(req.session)
    res.header("Access-Control-Allow-Origin", "*");
    console.log(req.session.username)
    data.executeQuery('SELECT l.id_liste, l.date_creation, nom FROM Liste l, Utilisateur u WHERE u.pseudonyme = $1 AND u.id_utilisateur = l.id_utilisateur', [req.session.username], (result) => {
        res.json({lists: result.rows})
    })
}) 

//Ajouter une liste d'un utilisateur 
//router.post ...


//Supprimer une liste d'un user
//router.delete

//Afficher les tâches d'un utilisateur 
router.get('/user/list/task/:id', (req, res) => {
    data.executeQuery(`SELECT t.id_tache, t.nom FROM Utilisateur u, Liste l, Tache t WHERE u.pseudonyme = $1 AND l.id_liste = $2 AND u.id_utilisateur = l.id_utilisateur AND l.id_liste = t.id_liste`,[req.session.username,req.params.id], (result) => {
        res.json({listes: result.rows})
    })
})

router.post('/user/list/addlist', (req, res) => {
   /* console.log("On est dans router.post")
    console.log(req.body)
    data.executeQuery(`INSERT INTO Liste(date_creation,nom,id_utilisateur) VALUES (CURRENT_DATE,'Donner à boire',$1)`  ,[req.session.userId], (result) => {
        res.end("Utilisateur ajouté")
    }) */
})  


module.exports = router