"use strict"

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const session = require('express-session')
const api = require('./api/api')
const auth = require('./routes/auth')
const welcome = require('./routes/welcome')
const create_account = require('./routes/create_account')
const profile = require('./routes/profile')
const logout = require('./routes/logout')
const board = require('./routes/board')

app.set("view engine", "pug")

const sessionParams = {
    secret: "my_secret",
    saveUninitialized: true,
    maxAge: 24 * 60 * 60 * 1000
  }

  app.use(session(sessionParams))
  
  app.use((req, res, next) => {
    if (req.session && req.session.userId) {
      res.locals.username = req.session.username  
    }
    next()
  })


app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({extended:true}))
app.use(express.static((__dirname, 'public')))


app.use('/api', api)
app.use('/', auth)
app.use('/create_account', create_account)
app.use('/welcome', welcome)
app.use('/profile', profile)
app.use('/logout', logout)
app.use('/board', board)


app.listen(3000, () => {
    console.log("server just started on port 3000");
})

