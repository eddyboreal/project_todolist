"use strict";

const express = require('express')
const router = express.Router()


router.get('/', (req, res) => {
    req.session.destroy()
    console.log('session déconnectée')
    res.render('logout')
})



module.exports = router