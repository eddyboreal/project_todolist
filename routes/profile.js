"use strict";

const express = require('express')
const router = express.Router()
const data = require('../db/data.js')



router.get('/', (req, res) => {
    let user = null
    console.log(req.session)
    data.executeQuery('Select pseudonyme, email, avatar from Utilisateur Where pseudonyme = $1', [req.session.username], (result) =>{
        user = result.rows  
        let path = 'file://' + __dirname
        const new_path = path.replace('routes', '') 

        res.render('profile', {pseudo: user[0].pseudonyme, mail: user[0].email, avatar: new_path + 'avatars/' + user[0].avatar})
    })
})

router.post('/', (req, res) => {
    let unsecured_password = null
    let pseudo_changed = null
    data.executeQuery('Select * from Utilisateur WHERE pseudonyme = $1', [req.session.username], (res) =>{
        unsecured_password = res.rows[0].mot_de_passe


        if(req.body.useractualpassword == unsecured_password){
            if(req.body.usermail){
                data.executeQuery('UPDATE Utilisateur SET email = $1 WHERE pseudonyme = $2', [req.body.usermail, req.session.username], (result1) =>{
                    console.log('email updated')
                })
            }
    
            if(req.body.userpseudo){

                data.executeQuery('Select * from Utilisateur', [], (resquery) => {
                    let flag = null
                    const users = resquery.rows
                    users.forEach(user => {
                        if(user.pseudonyme == req.body.userpseudo){
                            flag = false
                            return
                        }
                    })
                    if(flag == null){
                        data.executeQuery('UPDATE Utilisateur SET pseudonyme = $1 WHERE pseudonyme = $2', [req.body.userpseudo, req.session.username], (result1) =>{
                            pseudo_changed = true
                            console.log('pseudo updated')
                        })
                    }
                    else{
                        console.log('Insertion impossible, pseudo déjà pris')
                        res.redirect('profile')
                    }
                })      
            }
    
    
            if(req.body.useractualpassword && req.body.userpassword ){
                data.executeQuery('UPDATE Utilisateur SET mot_de_passe = $1 WHERE pseudonyme = $2', [req.body.userpassword, req.session.username], (result1) =>{
                    console.log('password updated')
                })
            }
        }
        else{
            console.log('erreur')
        }
    
    })

    if(pseudo_changed == true){
        // update of the req.session.username in case pseudo has updated
        req.session.username = req.body.userpseudo
    }

    res.render('profile', {pseudo: req.body.userpseudo, mail: req.body.usermail})

})





module.exports = router