"use strict";
	
    const express = require('express')
	const router = express.Router()
	const data = require('../db/data.js')
	
	router.get('/', (req, res) => {
	    res.render('board')
	})
	
	router.post('/', (req, res) => {
		//insertion de la liste créée par le form 
		if(req.body.postValue){
			data.executeQuery('INSERT INTO Liste (date_creation, nom, id_utilisateur) VALUES (CURRENT_DATE, $1, $2)', [req.body.postValue, req.session.userId], (result) =>{
				console.log('insert db')
			})
		}
		res.redirect('board')
	})
		
	
module.exports = router