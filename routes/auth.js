"use strict";

const express = require('express')
const router = express.Router()
const data = require('../db/data.js')

router.get('/', (req, res) => {
    res.render('auth')
})


router.post('/', (req, res) => {
    let userFound = null
    let resultMessage = ''
    data.executeQuery('Select * from Utilisateur', [], (result) => {
        const users = result.rows
        
        users.forEach(user => {
            if (user.email === req.body.mail
             && user.mot_de_passe === req.body.password) {
              userFound = user
              console.log(userFound)
              return
            }
          })

          if (userFound) {
            console.log('utilisateur trouvé !')
          data.executeQuery('Select id_utilisateur from Utilisateur where email = $1', [req.body.mail], (result) => {
              req.session.userId = result.rows[0].id_utilisateur
              req.session.save()
          })

          req.session.username = userFound.pseudonyme
          res.locals.username = userFound.pseudonyme
          req.session.userId = userFound.id_utilisateur
    

          resultMessage = `Vous êtes connecté en temps que ${userFound.pseudonyme}`
 
          res.render('board')
        }
        else {
          resultMessage = 'L\'identifiant ou le mot de passe sont incorrects'
          res.render('auth',{message : resultMessage})
        }
        console.log(resultMessage)
    })
}) 


module.exports = router