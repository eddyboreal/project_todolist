"use strict";

const express = require('express')
const router = express.Router()
const data = require('../db/data.js')
const nodemailer = require('nodemailer')


//permet de stocker l'avatar dans notre disque
//seul le path vers l'avatar est transmis à la database
const multer = require('multer')
const multerConf = {
    storage : multer.diskStorage({
        destination : function(req, file, next){
            next(null, './avatars')
        },
        filename : function(req, file, next){
            console.log(file)
            const extension = file.mimetype.split('/')[1]
            next(null, req.body.pseudo + '.' + extension)
        }
    })
}


const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'teameasytodolist@gmail.com',
        pass: 'easypassword'
    }
});


router.get('/', (req, res) => {
    res.render('create_account')
})



router.post('/welcome', multer(multerConf).single('avatar'), (req, res) => {
    data.executeQuery('Select * from Utilisateur', [], (resquery) => {
        let flag = null
        const users = resquery.rows
        users.forEach(user => {
            if(user.pseudonyme === req.body.pseudo){
                flag = false
                return
            }
        })
        if(flag === null){
            if(req.file){
                req.body.avatar = req.file.filename
            }
            data.executeQuery('INSERT INTO Utilisateur (pseudonyme, mot_de_passe, date_naissance, email, avatar) VALUES ($1, $2, $3, $4, $5)', [req.body.pseudo, req.body.password, req.body.birthdate, req.body.mail, req.body.avatar], (result) => {
                console.log("insertion de l'utilisateur terminée")

            })
            let mailOptions = {
                from: 'teameasytodolist@gmail.com', 
                to: req.body.mail, 
                subject: `EasyToDoList Inscription` , 
                html: `<p>Bienvenue ${req.body.pseudo} ! </br> La Team EasyToDoList vous confirme votre inscription sur notre plateforme. Bonne navigation sur notre site.</p>`// plain text body
            };
        
            transporter.sendMail(mailOptions, function(error, info){
                if(error){
                   return console.log(error);
                }
                console.log('Message sent: ' + info.response);
           });
           
            transporter.close();
        
            res.render('welcome', {pseudo: req.body.pseudo})
        }
        else{
            console.log('Insertion impossible, pseudo déjà pris')
            res.redirect('create_account')
        }
    })


})

module.exports = router