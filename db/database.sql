DROP TABLE IF EXISTS  Utilisateur, Liste, Tache;


CREATE TABLE Utilisateur(
  id_utilisateur SERIAL PRIMARY KEY,
  pseudonyme VARCHAR(25),
  mot_de_passe VARCHAR(50),
  avatar VARCHAR(50),
  date_naissance DATE,
  email VARCHAR(50)
);

INSERT INTO Utilisateur (pseudonyme, mot_de_passe, date_naissance, email)
VALUES ('lulu_d', 'vert', '12/04/1999', 'lulu.dolan@gmail.com');

INSERT INTO Utilisateur (pseudonyme, mot_de_passe, date_naissance, email)
VALUES ('aldeux_n', 'noir', '10/23/1996', 'alain-nguyen@hotmail.com');

INSERT INTO Utilisateur (pseudonyme, mot_de_passe, date_naissance, email)
VALUES ('eded_a', 'rouge', '03/17/1998', 'eded@yahoo.com');

INSERT INTO Utilisateur (pseudonyme, mot_de_passe, date_naissance, email)
VALUES ('eliott_r', 'gris', '01/29/1995', 'eliottR@laposte.net');


CREATE TABLE Liste(
  id_liste SERIAL PRIMARY KEY,
  date_creation DATE,
  nom VARCHAR(50),
  id_utilisateur INT REFERENCES Utilisateur(id_utilisateur)
);

INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Faire le projet web', 1);
INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Réviser tout le week-end', 1);
INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Survivre à la semaine prochaine', 1);


INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Projets persos', 2);
INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Jeux', 2);
INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Amis', 2);

INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Livres', 3);
INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Juridique', 3);

INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Films à voir', 4);
INSERT INTO Liste (date_creation, nom, id_utilisateur)
VALUES (CURRENT_DATE, 'Séries à binge watcher', 4);


CREATE TABLE Tache(
  id_tache SERIAL PRIMARY KEY,
  nom VARCHAR(100),
  detail VARCHAR(255),
  date_creation DATE,
  date_echeance DATE,
  etat VARCHAR(100),
  id_liste INT REFERENCES Liste(id_liste)
);


INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Se répartir les tâches', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 1);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Faire les pages webs', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 1);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Gérer les cookies', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 1);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Authentification', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 1);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Utiliser bootstrap', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 1);


INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Rattraper tout le cours de maths', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 2);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Réviser les designs patterns', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 2);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Revoir les tps en réseaux', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 2);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Préparer le ppp...', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 2);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Mettre le réveil lundi...', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 3);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Mettre le réveil mardi...', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 3);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Mettre le réveil mercredi...', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 3);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('Mettre le réveil jeudi...', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 3);

INSERT INTO Tache (nom, detail, date_creation, date_echeance, etat, id_liste)
VALUES ('On a compris...', 'lore ipsum', CURRENT_DATE, '03/13/2020', 'to do', 3);