Pour lançer le serveur :
    - npm start

La database peut être importée via la commande node db/import.js
Le script SQL associé est db/database.sql

Le board n'est pas encore fonctionnel

Fonctionnalités validées à ce jour :
    - authentification
    - création du compte
    - envoie d'un mail de confirmation
    - modification du profil
    - déconnexion 
    - routage safe (pas de données sensible dans les URLs)
    - ajout de liste dans la base utilisateur
    - affichage des listes et des tâches